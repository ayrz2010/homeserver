package main

import (
	"fmt"
	"adai.design/homeserver/devices"
	"adai.design/homeserver/tool"
	"adai.design/homeserver/members"
	"adai.design/homeserver/home"
)

func main() {
	fmt.Println("let's go!")
	end := make(chan bool, 1)
	devices.StartService(end)

	tool.Start(end)
	members.Start(end)
	home.Establish(end)

	<- end
}
