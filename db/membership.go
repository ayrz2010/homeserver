package db

import (
	"encoding/json"
	"time"
	"github.com/globalsign/mgo/bson"
	"strings"
	"adai.design/homeserver/log"
)

const (
	MembershipRoleMaster = "master"
	MembershipRoleGuest = "guest"
)

// 成员的家庭以及在家庭中的角色
type MembershipHome struct {
	Id 		string		`json:"id" bson:"id"`
	Role 	string		`json:"role" bson:"role"`
}

const (
	MembershipClientTypePhone = "iphone"
	MembershipClientTypePad = "ipad"
	MembershipClientMacbook = "macbook"
	MembershipClientWeb = "web"
)

// 成员与服务器进行连接的客户端信息
type MembershipClient struct {
	DevType 	string		`json:"dev_t"`
	Session 	string		`json:"session" bson:"session"`
	// iOS设备推送Token
	APNS 		string		`json:"apns" bson:"apns"`
	// 最后一次登录的时间
	Last  		time.Time	`json:"last" bson:"last"`
	// 推送消息数量
	Badge 		int			`json:"badge" bson:"badge"`
}

type Membership struct {
	Id 		string		`json:"id" bson:"id"`
	Name 	string		`json:"name" bson:"name"`
	Icon 	string		`json:"icon,omitempty" bson:"icon,omitempty"`

	Phone 		string		`json:"phone" bson:"phone"`
	Email   	string		`json:"email" bson:"email"`
	Password 	string		`json:"password" bson:"password"`

	Homes 		[]*MembershipHome	`json:"homes,omitempty" bson:"homes,omitempty"`
	Clients 	[]*MembershipClient	`json:"clients,omitempty" bson:"clients,omitempty"`
}

func (m *Membership) Save() error {
	session, err := getSession()
	if err != nil {
		return err
	}
	defer session.Close()
	collection := session.DB(dbName).C(collectionMembership)
	// 更新插入
	err = collection.Update(bson.M{"id": m.Id}, m)
	if err != nil {
		return err
	}
	return nil
}

func (m *Membership) String() string {
	buf, _ := json.Marshal(m)
	return string(buf)
}

// 通过ID查找成员
func GetMembershipById(id string) *Membership {
	session, err := getSession()
	if err != nil {
		return nil
	}
	defer session.Close()

	collection := session.DB(dbName).C(collectionMembership)
	var membership Membership
	err = collection.Find(bson.M{"id": id}).One(&membership)
	if err != nil {
		return nil
	}
	return &membership
}

//  通过账号查找成员(邮箱或者手机号)
func GetMembershipByAccount(account string) *Membership {
	if account == "" {
		return nil
	}
	var query bson.M
	if strings.Contains(account, "@") {
		query = bson.M{"email": account}
	} else {
		query = bson.M{"phone": account}
	}

	session, err := getSession()
	if err != nil {
		return nil
	}
	defer session.Close()

	collection := session.DB(dbName).C(collectionMembership)
	var membership Membership
	err = collection.Find(query).One(&membership)
	if err != nil {
		return nil
	}
	return &membership
}

// 成员登录在线信息
const (
	MembershipStateOnline = "online"
	MembershipStateOffline = "offline"
)

type MembershipStatus struct {
	Id 			string		`json:"id"`
	State 		string		`json:"state"`
	ClientType 	string	`json:"dev_type"`
	Time 		time.Time	`json:"-" bson:"time"`
	Date 		string		`json:"date" bson:"-"`
	IP 			string		`json:"ip" bson:"ip"`
}

func (m *Membership) Online(c *MembershipClient, ip string) {
	session, err := getSession()
	if err != nil {
		return
	}
	defer session.Close()
	collection := session.DB(dbName).C(collectionMembershipLog)

	status := &MembershipStatus{
		Id: m.Id,
		State: MembershipStateOnline,
		ClientType: c.DevType,
		Time: time.Now(),
		IP: ip,
	}
	err = collection.Insert(status)
	if err != nil {
		log.Error("err: %s", err)
	}
}

func (m *Membership) Offline(c *MembershipClient, ip string) {
	session, err := getSession()
	if err != nil {
		return
	}
	defer session.Close()
	collection := session.DB(dbName).C(collectionMembershipLog)

	status := &MembershipStatus{
		Id: m.Id,
		State: MembershipStateOffline,
		ClientType: c.DevType,
		Time: time.Now(),
		IP: ip,
	}
	err = collection.Insert(status)
	if err != nil {
		log.Error("err: %s", err)
	}
}




