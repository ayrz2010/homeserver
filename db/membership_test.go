package db

import (
	"testing"
	"github.com/globalsign/mgo/bson"
	"time"
)

// 插入设备
func TestMembershipInsert(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	member := &Membership{
		Id: "fc2f45a8-abe2-47a1-8965-4161baf09d68",
		Name: "刀刀",
		Icon: "daodao",
		Phone: "20180325",
		Email: "",
		Password: "123456",
		Homes: []*MembershipHome{
			{Id: "65904a3d-d5cd-47d7-9c3e-3bd05317cc53", Role: MembershipRoleGuest},
		},
	}

	collection := session.DB(dbName).C(collectionMembership)
	// 当前成员是否已经存在
	if n, err := collection.Find(bson.M{"id": member.Id}).Count(); err == nil && n != 0 {
		t.Fatalf("membership(%s) has already existed", member.Id)
	}

	// 插入设备
	err = collection.Insert(member)
	if err != nil {
		t.Fatal(err)
	}

	// 查找插入的设备
	var m *Membership
	err = collection.Find(bson.M{"id": member.Id}).One(&m)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(m.String())
}


func TestMembershipUpsert(t *testing.T) {
	session, err := getSession()
	if err != nil {
		t.Fatal(err)
	}
	defer session.Close()

	member := &Membership{
		Id: "fc2f45a8-abe2-47a1-8965-4161baf09d68",
		Name: "光头云",
		Icon: "men",
		Phone: "18565381403",
		Email: "zyclouds@gmail.com",
		Password: "123456",
		Homes: []*MembershipHome{
			{Id: "65904a3d-d5cd-47d7-9c3e-3bd05317cc53", Role: MembershipRoleMaster},
		},
		Clients: []*MembershipClient {
			{
				DevType: MembershipClientTypePhone,
				Session: "5a4c2089bfce4ebe15164d3ce44744d5",
				APNS: "f0c054609c6501525f0d17db21abfdab1fa93a7a38dad8aa96db7f31f5afccec",
				Last: time.Now(),
				Badge: 0,
			},
		},
	}

	collection := session.DB(dbName).C(collectionMembership)
	// 插入设备
	info, err := collection.Upsert(bson.M{"id": member.Id}, member)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("upsert info: matched(%v) removed(%v) updated(%v)", info.Matched, info.Removed, info.Updated)
	// 查找插入的设备
	var m *Membership
	err = collection.Find(bson.M{"id": member.Id}).One(&m)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(m.String())
}

func TestFindMembershipById(t *testing.T) {
	id := "34c92c64-6d84-490f-8e25-e27e1c8bf58e"
	membership := GetMembershipById(id)
	if membership == nil {
		t.Fatal("404 not found")
	}
	t.Log(membership.String())
}

func TestFindMembershipByEmail(t *testing.T) {
	account := "785492565@qq.com"
	membership := GetMembershipByAccount(account)
	if membership == nil {
		t.Fatal("404 not found")
	}
	t.Log(membership.String())
}

func TestFindMembershipByPhone(t *testing.T) {
	account := "18565381403"
	membership := GetMembershipByAccount(account)
	if membership == nil {
		t.Fatal("404 not found")
	}
	t.Log(membership.String())
}



