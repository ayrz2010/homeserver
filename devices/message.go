package devices

import "encoding/json"

type Message struct {
	Path	string		`json:"path"`
	Method 	string		`json:"method"`
	State 	string		`json:"state,omitempty"`
	Data	json.RawMessage		`json:"data,omitempty"`
}

func (m *Message) String() string {
	buf, _ := json.Marshal(m)
	return string(buf)
}

const MessageSlice = '\n'

const (
	msgPathLogin          = "login"
	msgPathHeartbeat      = "hb"
	MsgPathAccessories    = "accessories"
	MsgPathCharacteristic = "characteristic"
	MsgPathContainer 	  = "container"
)

const (
	MsgMethodPost = "post"
	MsgMethodGet = "get"
	MsgMethodPut = "put"
)

// devices <<--->> home之间传递消息的结构体
type MessagePkg struct {
	DevId 		string
	HomeId 		string
	Msg			*Message
}

const (
	ContainerStateOnline = "online"
	ContainerStateOffline = "offline"
	ContainerStateAdd = "add"
	ContainerStateDel = "del"
	ContainerStateUpdate = "update"
)