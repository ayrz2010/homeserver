package devices

import (
	"encoding/json"
	"errors"
	"adai.design/homeserver/db"
)

type loginInfo struct {
	UUID 	string		`json:"uuid"`
	Key 	string		`json:"key"`
	Date 	string		`json:"date"`
}

// 设备登录认证信息
func login(data []byte) (*db.Register, error) {
	var info loginInfo
	err := json.Unmarshal(data, &info)
	if err != nil {
		return nil, err
	}

	if len(info.UUID) == 0 || len(info.Key) == 0 {
		return nil, errors.New("invalid login information")
	}

	register, err := db.GetRegisterById(info.UUID)
	if register == nil || err != nil {
		return nil, errors.New("unregister device")
	}

	if register.PrivateKey != info.Key {
		return nil, errors.New("invalid private key")
	}
	return register, nil
}













