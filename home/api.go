package home

import "adai.design/homeserver/members"

const (
	PathHome = "home"
	PathContainer = "home/container"
	PathContainerCharacteristic = "home/container/characteristic"
	PathCharacteristic = "home/characteristic"

	PathService = "home/service"
	PathRoom = "home/room"
	PathMember = "home/member"

	PathScene = "home/scene"
	PathAutomation = "home/automation"
	PathLocation = "home/location"
	PathStatisticPush = "home/statistic/push"
	PathStatisticCharacteristic = "home/statistic/characteristic"
	PathStatisticLocation = "home/statistic/location"
)

type apiInterface interface {
	Handle(h *Home, pkg *members.MessagePkg) error
}

