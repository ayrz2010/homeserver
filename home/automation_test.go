package home

import (
	"testing"
	"time"
	"adai.design/homeserver/members"
	"crypto/tls"
	"github.com/sideshow/apns2/certificate"
	"encoding/json"
	"adai.design/homeserver/home/data"
)

func TestAutoParse(t *testing.T) {
	home, err := FindHomeById(testHomeId)
	if err != nil {
		t.Fatal(err)
	}

	home.containerManager = &ContainerManager{containers: home.Containers}
	for _, trigger := range home.triggerManager.triggers {
		t.Logf("title:%s enable(%v)\n", trigger.Title, trigger.IsEnable)
		t.Logf("\tsubtitle:%s\n",trigger.SubTitle)
	}
}

func TestAutoTimerTrigger(t *testing.T) {
	home, err := FindHomeById(testHomeId)
	if err != nil {
		t.Fatal(err)
	}

	id := "8f03b04b-5feb-4772-96cf-0a6dc84df5e9"
	trigger := home.triggerManager.findTriggerById(id)
	if trigger == nil {
		t.Fatalf("trigger(%v) not found", id)
	}

	for _, event := range trigger.Events {
		if event.Type == AutoTimer {
			if timer, ok := event.data.(*eventTimer); ok {
				t.Logf("trigger(%s)\n", trigger.Title)
				t.Logf("\ttimer(%d:%d) week(%x)\n", timer.MinuteOfDay/60, timer.MinuteOfDay%60, timer.Week)
			}
		}
	}

	members.APNSCert = func() *tls.Certificate {
		cert, _ := certificate.FromP12File("../conf/home.p12", "clouds")
		return &cert
	}()

	// 7:10 触发
	now, err := time.Parse("2006/01/02 15:04:05", "2018/05/07 07:10:00")
	if err != nil {
		t.Fatal(err)
	}
	if trigger.checkTimerTrigger(home, now) != true {
		t.Fatalf("check err at(%s)", now.Format("15:04:05"))
	}

	// 7:11 触发
	now = now.Add(time.Minute * 1)
	if trigger.checkTimerTrigger(home, now) == true {
		t.Fatalf("check err at(%s)", now.Format("15:04:05"))
	}
	t.Log("ok")
}

func TestAutoEventTimer(t *testing.T) {
	// 周一
	now, err := time.Parse("2006/01/02 15:04:05", "2018/05/07 17:01:14")
	if err != nil {
		t.Fatal(err)
	}
	t.Log("now: ", now)

	event := &eventTimer{
		MinuteOfDay: now.Minute() + now.Hour() * 60,
		Week: 0x3e,	// 工作日,周一至周五
	}

	if title, want := event.title(), "下午 17:01"; title != want {
		t.Fatalf("title(%s) want(%s)", title, want)
	}

	// 周一至周五
	for i:=0; i<5; i++ {
		timer := now.Add(time.Hour * time.Duration(24) * time.Duration(i))
		if want, ok := true, event.check(timer); want != ok {
			t.Fatalf("week(%d) check error\n", timer.Weekday())
		}
	}

	// 周末(周六、周日)
	for i:=5; i<7; i++ {
		timer := now.Add(time.Hour * time.Duration(24) * time.Duration(i))
		if want, ok := false, event.check(timer); want != ok {
			t.Fatalf("week(%d) check error\n", timer.Weekday())
		}
	}

	// 后一分钟
	timer := now.Add(time.Minute)
	if want, ok := false, event.check(timer); want != ok {
		t.Fatalf("time(%d:%d) check error\n", timer.Hour(), time.Minute)
	}

	//前一分钟
	timer = now.Add(-time.Minute)
	if want, ok := false, event.check(timer); want != ok {
		t.Fatalf("time(%d:%d) check error\n", timer.Hour(), time.Minute)
	}
}

func TestAutoCharacteristicTrigger(t *testing.T) {
	members.APNSCert = func() *tls.Certificate {
		cert, _ := certificate.FromP12File("../conf/home.p12", "clouds")
		return &cert
	}()

	home, err := FindHomeById(testHomeId)
	if err != nil {
		t.Fatal(err)
	}

	id := "77c49577-15ba-4bd2-ab85-75252e28e426"
	trigger := home.triggerManager.findTriggerById(id)
	if trigger == nil {
		t.Fatalf("trigger(%v) not found", id)
	}

	for _, event := range trigger.Events {
		if event.Type == AutoCharacteristic {
			if char, ok := event.data.(*eventCharacteristic); ok {
				t.Logf("trigger(%s)\n", char.title(home))
			}
		}
	}

	current := home.containerManager.getCharacteristic("f241cd80e3434072", 1, 2)
	buf, err := json.Marshal(current)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("characteristic: ", string(buf))

	buf = []byte(`{"aid":"f241cd80e3434072","sid":1,"cid":2,"value":80}`)
	var now data.Characteristic
	err = json.Unmarshal(buf, &now)
	if err != nil {
		t.Fatal(err)
	}

	if trigger.checkCharacteristicTrigger(home, &now) != true {
		t.Fatalf("check characteristic failed info(%s)", string(buf))
	}

	buf = []byte(`{"aid":"f241cd80e3434072","sid":1,"cid":2,"value":79}`)
	err = json.Unmarshal(buf, &now)
	if err != nil {
		t.Fatal(err)
	}
	if trigger.checkCharacteristicTrigger(home, &now) != false {
		t.Fatalf("check characteristic failed info(%s)", string(buf))
	}
}




