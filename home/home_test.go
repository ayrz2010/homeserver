package home

import (
	"testing"
	"io/ioutil"
	"encoding/json"
	"errors"
	"adai.design/homeserver/db"
	"github.com/globalsign/mgo/bson"
	"time"
)

const testHomeId = "65904a3d-d5cd-47d7-9c3e-3bd05317cc53"

func FindHomeById(id string) (*Home, error) {
	buf, err := ioutil.ReadFile("../conf/home.json")
	if err != nil {
		panic(err)
	}

	var homes []*Home
	if err = json.Unmarshal(buf, &homes); err != nil {
		panic(err)
	}

	for _, home := range homes {
		if home.Id == id {
			home.containerManager = &ContainerManager{containers: home.Containers}
			home.serviceManager = &ServiceManager{services: home.Services}
			home.sceneManager = &SceneManager{scenes: home.Scenes}
			home.memberManager = &MemberManager{members: home.Members}
			home.triggerManager = &TriggerManager{triggers: home.Triggers}
			return home, nil
		}
	}
	return nil, errors.New("404 Not Found")
}

func TestHomeParse(t *testing.T) {
	home, err := FindHomeById(testHomeId)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("home:", home.Id)
	t.Log("name:", home.Name)

	for i, m := range home.Members {
		t.Logf("member[%d]: %s rule(%s)\n", i, m.Id, m.Role)
	}

	for i, r := range home.Rooms {
		t.Logf("room[%d]: %s name(%s)\n", i, r.Id, r.Name)
	}

	for i, s := range home.Services {
		t.Logf("service[%d] id(%s.%d) name(%s)\n", i, s.AId, s.SId, s.Name)
	}

}

func TestHomeInsert(t *testing.T) {
	home, err := FindHomeById(testHomeId)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("home:", home.Id)
	t.Log("name:", home.Name)

	for i, s := range home.sceneManager.scenes {
		t.Logf("scene[%d] id(%s) name(%s)\n", i, s.Id, s.Name)
		for i, a := range s.Actions {
			t.Logf("\taction[%d] target(%s.%d.%d] value(%v)\n", i, a.AId, a.SId, a.CId, a.Value)
		}
	}

	session, collection := db.GetHomeSession()
	if collection == nil {
		t.Fatal("db collection 404 not found")
	}
	defer session.Close()
	collection.Upsert(bson.M{"id": home.Id}, home)
}

func TestHomeSceneFind(t *testing.T) {
	var home Home
	session, collection := db.GetHomeSession()
	if collection == nil {
		t.Fatal("db collection 404 not found")
	}
	defer session.Close()

	err := collection.Find(bson.M{"id": "65904a3d-d5cd-47d7-9c3e-3bd05317cc53"}).One(&home)
	if err != nil {
		t.Fatal(err)
	}
	home.containerManager = &ContainerManager{containers: home.Containers}
	home.serviceManager = &ServiceManager{services: home.Services}
	home.sceneManager = &SceneManager{scenes: home.Scenes}
	home.memberManager = &MemberManager{members: home.Members}
	home.triggerManager = &TriggerManager{triggers: home.Triggers}

	go home.run(nil)
	//time.Sleep(time.Microsecond * 10)
	//buf, err := json.Marshal(home.triggerManager.triggers)
	//if err != nil {
	//	t.Fatal(err)
	//}
	//t.Log(string(buf))

	time.Sleep(time.Microsecond * 10)
	buf, err := json.Marshal(home.containerManager.containers)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(buf))

}


func TestHomeContainer(t *testing.T)  {
	home, err := FindHomeById(testHomeId)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("home:", home.Id)
	t.Log("name:", home.Name)

	for i, c := range home.Containers {
		t.Logf("container[%d] id(%s) version(%d)\n", i, c.Id, c.Version)
		for i, a := range c.Accessories {
			t.Logf("\taccessory[%d] id(%s)\n", i, a.UUID)
		}
	}
}

func TestHomeExecute(t *testing.T) {
	home, err := FindHomeById(testHomeId)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("home:", home.Id)
	t.Log("name:", home.Name)

	s := home.sceneManager.findSceneById("43ddede5-7a9f-4b7d-878f-d4be3e349b61")
	if s == nil {
		t.Fatal("scene 404 not found")
	}

	container := home.containerManager.findContainerById("f241cd80e3434072")
	if container == nil {
		t.Fatal("container 404 not found")
	}
	container.IsReachable = true

	err = s.execute(home)
	if err != nil {
		t.Fatal(err)
	}
}
