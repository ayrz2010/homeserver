package model

const (
	UnitPercentage 	= "percentage"
	UnitArcDegrees 	= "arcdegrees"
	UnitCelsius 	= "celsius"
	UnitLux			= "lux"
	UnitSeconds		= "seconds"
)

const (
	FormatString 	= "string"
	FormatBool 		= "bool"
	FormatFloat		= "float"
	FormatUInt8		= "uint8"
	FormatUInt16 	= "uint16"
	FormatUInt32	= "uint32"
	FormatInt32		= "int32"
	FormatAnyObject = "any"
	FormatTLV8		= "tlv8"
)

const (
	TypeName 				= "1"
	TypeModel 				= "3"
	TypeManufacturer 		= "2"
	TypeSerialNumber		= "4"
	TypeFirmwareRevision 	= "5"

	TypeCurrentTemperature 			= "11"
	TypeCurrentRelativeHumidity 	= "12"
	TypeCurrentAmbientLightLevel	= "13"
	TypeCurrentAmbientInfraredLevel = "14"

	TypeContactSensorState			= "15"
	TypeOn 							= "16"
	TypeTargetTemperature			= "17"
	TypeAirConditionerMode			= "18"
	TypeAirConditionerRotationMode  = "19"
	TypeAirConditionerWindSpeed     = "1A"

	TypeScreenBrightness	= "1B"
	TypeCurrentTime			= "1C"
	TypeVolume				= "1D"
)

type Characteristic struct {
	Id 			int			`json:"cid" bson:"cid"`
	Type 		string			`json:"type" bson:"type"`
	Value 		interface{}		`json:"value,omitempty" bson:"value,omitempty"`

	Format 		string			`json:"format" bson:"format"`
	Unit 		string			`json:"unit,omitempty" bson:"unit,omitempty"`

	MaxValue	interface{}		`json:"max,omitempty" bson:"max,omitempty"`
	MinValue	interface{}		`json:"min,omitempty" bson:"min,omitempty"`
	StepValue	interface{}		`json:"step,omitempty" bson:"step,omitempty"`
}