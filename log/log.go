//
// +build !mipsle

package log

import (
	"sync"
	"io"
	"os"
	"time"
	"runtime"
	"fmt"
	"strings"
)

type Logger struct {
	mu sync.Mutex
	out io.Writer
}

func (l *Logger) Output(calldepth int, s string) error {
	now := time.Now().Format("01/02 15:04:05.99")
	l.mu.Lock()
	defer l.mu.Unlock()

	_, file, line, ok := runtime.Caller(calldepth)
	if !ok {
		file = "???"
		line = 0
	} else {
		short := strings.TrimPrefix(file, os.Getenv("GOPATH") + "src/adai.design/homeserver/")
		//short := file
		//for i := len(file) - 1; i > 0; i-- {
		//	if file[i] == '/' {
		//		short = file[i+1:]
		//		break
		//	}
		//}
		file = short
	}

	log := fmt.Sprintf("%s %s:%d %s", now, file, line, s)
	l.out.Write([]byte(log))
	return nil
}

var std = &Logger{out: os.Stderr}

func Printf(format string, v ...interface{}) {
	std.Output(2, fmt.Sprintf(format, v...))
}

func Println(v ...interface{}) {
	std.Output(2, fmt.Sprintln(v...))
}

func Debug(format string, v ...interface{}) {
	std.Output(2, fmt.Sprintf(format + "\n", v...))
}

func Info(format string, v ...interface{}) {
	std.Output(2, fmt.Sprintf("\033[32;1m" + format + "\033[0m\n", v...))
}

func Notice(format string, v ...interface{})  {
	std.Output(2, fmt.Sprintf("\033[33;1m" + format + "\033[0m\n", v...))
}

func Error(format string, v ...interface{})  {
	std.Output(2, fmt.Sprintf("\033[31;1m" + format + "\033[0m\n", v...))
}

func Fatal(v ...interface{}) {
	std.Output(2, fmt.Sprint(v...))
	os.Exit(1)
}

func Panic(v ...interface{}) {
	s := fmt.Sprint(v...)
	std.Output(2, s)
	panic(s)
}

func Panicln(v ...interface{}) {
	s := fmt.Sprintln(v...)
	std.Output(2, s)
	panic(s)
}




