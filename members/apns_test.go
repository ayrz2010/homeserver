package members

import (
	"testing"
	"github.com/sideshow/apns2/certificate"
	"github.com/sideshow/apns2"
)

func Test_APNSClient(t *testing.T) {
	cert, err := certificate.FromP12File("../conf/apns.p12", "clouds")
	if err != nil {
		t.Fatal("cert err: ", err)
	}

	notification := &apns2.Notification{
		DeviceToken: "f0c054609c6501525f0d17db21abfdab1fa93a7a38dad8aa96db7f31f5afccec",
		Topic: "adai.design.home",
	}

	notification.Payload = []byte(`{
		"aps" : {
			"alert" : {
				"title" : "7点整回家",
				"body" : "今天已经是12月25号了，你知道吗？"
			},
			"category":"GENERAL",
			"badge" : 1,
			"sound":"default"
	    }
	}`)

	client := apns2.NewClient(cert).Development()
	res, err := client.Push(notification)
	if err != nil {
		t.Fatal("result err:", err)
	}

	t.Logf("result code(%v) apnsId(%v) reason(%v)", res.StatusCode, res.ApnsID, res.Reason)
}
