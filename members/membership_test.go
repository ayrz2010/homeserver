package members

import (
	"testing"
	"github.com/satori/go.uuid"
	"encoding/json"
)

func TestUUID(t *testing.T) {
	id, err := uuid.NewV4()
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%X\n", id.Bytes())
	t.Logf("%x\n", id.Bytes())
}


func TestMemberShipIdLogin(t *testing.T) {
	// 账号+密码登录
	info := &loginInfo{
		Id: "34c92c64-6d84-490f-8e25-e27e1c8bf58e",
		Session: "5a4c2089bfce4ebe15164d3ce44744d5",
		DevType: "iphone",
	}
	data, _ := json.Marshal(info)
	m1, c1, err := login(data)
	if err != nil {
		t.Fatal(err)
	}

	cData, _ := json.Marshal(c1)
	mData, _ := json.Marshal(m1)
	t.Log("membership: ", string(mData))
	t.Log("client: ", string(cData))

	// ID+Session登录
	info = &loginInfo{
		Id: m1.Id,
		Session: c1.Session,
		DevType: c1.DevType,
	}
	data, _ = json.Marshal(info)
	m2, c2, err := login(data)
	if err != nil {
		t.Fatal(err)
	}

	if m2 != m1 && c2 != c1 {
		t.Fatal("duplicate login")
	}
}