package members

import "encoding/json"

const (
	msgPathLogin          = "login"
	msgPathHeartbeat      = "hb"

	MsgMethodPost = "post"
	MsgMethodGet = "get"
	MsgMethodPut = "put"
)

type Message struct {
	Path 		string		`json:"path"`
	Method 		string		`json:"method,omitempty"`
	Home 		string		`json:"home,omitempty"`
	Session 	string		`json:"session,omitempty"`
	State 		string		`json:"state,omitempty"`
	Data 		json.RawMessage		`json:"data,omitempty"`
}

func (m *Message) String() string {
	buf, _ := json.Marshal(m)
	return string(buf)
}


const (
	PkgTypeChannel = 1
	PkgTypeContext = 2
)

type Context struct {
	HomeId 		string		`json:"home"`
	MemberId 	string		`json:"member"`
	Session 	string		`json:"session"`
	Result 		chan *MessagePkg	`json:"-"`
}

type MessagePkg struct {
	Type 	int			`json:"type"`
	Ctx 	*Context	`json:"context"`
	Msg 	*Message	`json:"msg"`
}

func (pkg *MessagePkg) String() string {
	buf, _ := json.Marshal(pkg)
	return string(buf)
}

