package members

import (
	"testing"
	"github.com/gorilla/websocket"
	"time"
	"encoding/json"
)

func TestLogin(t *testing.T) {
	c, _, err := websocket.DefaultDialer.Dial("wss://adai.design:6667/membership", nil)
	if err != nil {
		t.Fatal(err)
	}
	defer c.Close()

	data := map[string]interface{}{
		"account": "18565381403",
		"password": "1234567",
		"date": time.Now().Format("2006-01-02 15:04:05"),
		"dev_t": "ios",
	}

	buf, _ := json.Marshal(data)

	msg := &Message{
		Path: msgPathLogin,
		Method: MsgMethodPost,
		Data: buf,
	}
	buf, _ = json.Marshal(msg)
	c.WriteMessage(websocket.BinaryMessage, buf)

	_, buf, err = c.ReadMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("result: ", string(buf))

	msg = &Message{
		Path: msgPathHeartbeat,
		Method: MsgMethodPost,
	}
	buf, _ = json.Marshal(msg)
	c.WriteMessage(websocket.BinaryMessage, buf)
	
	_, buf, err = c.ReadMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("result: ", string(buf))

	msg = &Message{
		Path: "home/container",
		Method: MsgMethodGet,
	}
	buf, _ = json.Marshal(msg)
	c.WriteMessage(websocket.BinaryMessage, buf)

	_, buf, err = c.ReadMessage()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("result: ", string(buf))
}