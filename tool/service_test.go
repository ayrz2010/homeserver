package tool

import (
	"testing"
	"io/ioutil"
	"github.com/astaxie/beego/httplib"
	"encoding/json"
)

const (
	username = "f241cd80e3434072"
	//username = "a4c2532b877846e8"
	password = "8kHNgONDQHKDwT73vYHqsw=="
	url = "http://adai.design:8080/workers"
	device = username
)

func request(msg *Message) ([]byte, error) {
	request := httplib.Post(url + "/" + device)
	request.SetBasicAuth(username, password)

	data, _ := json.Marshal(msg)
	request.Body(data)

	res, err := request.DoRequest()
	if err != nil {
		return nil, err
	}

	data, err = ioutil.ReadAll(res.Body)
	res.Body.Close()

	return data, nil
}

func TestAccessoriesGet(t *testing.T) {
	message := &Message {
		Path: MsgPathAccessories,
		Method: MsgMethodGet,
	}

	ack, err := request(message)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("body: ", string(ack))
}

func TestCharacteristicGet(t *testing.T)  {
	message := &Message {
		Path: MsgPathCharacteristics,
		Method: MsgMethodGet,
	}

	ack, err := request(message)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("body: ", string(ack))
}

func TestCharacteristicsOpen(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID: "63b14730",
		ServiceId: 1,
		CharacteristicId: 1,
		Value: 0,
	},{
		AccessoryID: "aa9f362b",
		ServiceId: 1,
		CharacteristicId: 1,
		Value: 1,
	},{
		AccessoryID: "aa9f362b",
		ServiceId: 2,
		CharacteristicId: 1,
		Value: 1,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path: MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data: data,
	}

	ack, err := request(msg)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("body: ", string(ack))
}

func TestCharacteristicsClose(t *testing.T) {
	chars := []*Characteristic{{
		AccessoryID: "63b14730",
		ServiceId: 1,
		CharacteristicId: 1,
		Value: 0,
	},{
		AccessoryID: "aa9f362b",
		ServiceId: 1,
		CharacteristicId: 1,
		Value: 0,
	},{
		AccessoryID: "aa9f362b",
		ServiceId: 2,
		CharacteristicId: 1,
		Value: 0,
	}}

	data, _ := json.Marshal(chars)
	msg := &Message{
		Path: MsgPathCharacteristics,
		Method: MsgMethodPost,
		Data: data,
	}

	ack, err := request(msg)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("body: ", string(ack))
}

func TestContainerGet(t *testing.T) {
	msg := &Message{
		Path: MsgPathContainer,
		Method: MsgMethodGet,
	}

	ack, err := request(msg)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("body: ", string(ack))
}







